.. Gifpy documentation master file, created by
   sphinx-quickstart on Wed Aug  1 07:00:53 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gifpy's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. class:: Giphy(token)
Represents the Giphy API with the provided token

.. attribute:: _token
The provided token. It is internaly used by the methods below

.. attribute:: _api
The url of the Giphy API. It is internaly used by the methods below

.. method:: search(query)
Returns a list of **Gifs**.

.. method:: random
Returns a random **Gif**.

.. method:: upload(file)
Upload the provided file to Giphy. Must be .gif. Returns the uploaded gif ID.

.. class:: Gif(data)
Represents the Gif object.

.. attribute:: id
The id of the Gif.

.. attribute:: title
The title of the Gif.

.. attribute:: url
Link to the Gif.

.. attribute:: short_url
Shorten version of the url.

.. attribute:: embed_url
URL for embeding.

.. attribute:: images
Image variants of the GIF.

.. class:: Images(data)
Represents the Images object.

.. attribute:: original
The original image.

.. attribute:: fixed_width
The 200px width version of the image. Useful for mobile.

.. attribute:: fixed:height
The 200px height version of the image. Useful for mobile.

.. attribute:: small_fixed_width
The 100px width version of the image. Useful for mobile

.. attribute:: small_fixed_height
The 100px height version of the image. Useful for mobile.

.. class:: Image(data)
Represents the Image object

.. attribute:: url
URL of the image. The difference between this and Gif.url is that this one leads to the "pure image", while Gif.url redirects to the normal page.

.. attribute:: width
Width of the image.

.. attribute:: height
Height of the image.

.. attribute:: size
Size of the image, in bytes

.. attribute:: webp
URL to the webp variant of the image.

.. attribute:: webp_size
Size of the webp variant, in bytes.

.. attribute:: mp4
URL to the mp4 variant of the image.

.. attribute:: mp4_size
Size of the mp4 variant, in bytes

.. method:: save(file)
Save the image to **file**. Must be .gif

..
..
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
