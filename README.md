Gifpy is a python wrapper around the Giphy API. It's main goal is to be easy to use and portable. The only dependencie is requests, which is a small library

**Instalation:**
``pip3 install gifpy``

**Example**
```py
from gifpy import Giphy # import the Giphy object
giphy = Giphy('Your token') # create a Giphy object, using token
gifs = giphy.search('keyword') # search gifs using a keyword. Returns a list of Gif objects
gif = gifs[5] # get the fifth gif of the list
print(gif.title) # get the gif title
print(gif.url) # get the gif url
gif = giphy['image id'] # get a Gif with the given gif id
gif.images.original.save('filename.gif') # save the original gif on your computer
gif.images.fixed_width.save('filename.gif') # save the fixed 200px width version of the gif on your computer
```

***Documentation***
See http://gifpy.readthedocs.io/
