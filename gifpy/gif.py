from .images import Images

class Gif:
	def __init__(self,data):
		self.id = data['id']
		self.title = data['title']
		self.url = data['url']
		self.short_url = data['bitly_url']
		self.embed_url = data['embed_url']
		self.images = Images(data['images'])
	
	def __repr__(self):
		return 'Gif %s'%(self.id)
