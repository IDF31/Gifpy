import requests
from io import BytesIO

class Image:
	def __init__(self,data):
		self.url = data['url']
		self.width = data['width']
		self.height = data['height']
		self.size = data['size']
		self.mp4 = data['mp4']
		self.mp4_size = data['mp4_size']
		self.webp = data['webp']
		self.webp = data['webp_size']
	
	def save(self,path):
		r = requests.get(self.url)
		with open(path,'bw+') as image:
			image.write(r.content)
