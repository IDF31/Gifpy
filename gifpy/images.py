from .image import Image

class Images:
	def __init__(self,data):
		self.fixed_height = Image(data['fixed_height'])
		self.fixed_width = Image(data['fixed_width'])
		self.fixed_height_small = Image(data['fixed_height_small'])
		self.fixed_width_small = Image(data['fixed_width_small'])
		self.original = Image(data['original'])
