import requests
from .gif import Gif

class Giphy:
	def __init__(self,token):
		self._token = token
		self._api = 'http://api.giphy.com/v1/gifs/'
	def search(self,query,limit=25):
		r = requests.get(self._api+'search?api_key=%s&q=%s&limit=%s'%(self._token,query,limit))
		results = []
		for data in r.json()['data']:
			results.append(Gif(data))
		
		return results
	
	def random(self):
		r = requests.get(self._api+'random?api_key=%s'%(self._token))
		return Gif(r.json()['data'])
	
	def upload(self,path):
		r = requests.post('http://upload.giphy.com/v1/gifs',data={'api_key':self._token}, files={'file':open(path,'rb')})
		return r.json()['data']['id']


	def __getitem__(self,id):
		r = requests.get(self._api+'%s?api_key=%s'%(id,self._token))	
		return Gif(r.json()['data'])
