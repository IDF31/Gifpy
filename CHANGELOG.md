***Gifpy 1.0***
* ``Gif`` object now returns the Gif id as string
* Gifs can now be uploaded using the Giphy Upload API
* A command has been added for uploading gifs from the command line

***Gifpy 0.5.2***
* Added ``random()`` method for ``Giphy`` class, which return a random ``Gif``
* Added ``__getitem__`` to ``Giphy`` class, which returns a ``Gif`` with the given id 

***Gifpy 0.5***
* First version on PyPi
* Added ``save()`` method to the ``Image`` class
* First commits
